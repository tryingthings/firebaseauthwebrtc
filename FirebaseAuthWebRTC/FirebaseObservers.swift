//
//  FirebaseObservers.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 29/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import WebRTC

class FirebaseObservers: NSObject {
    
    static func listenForAnxious() {
        print("Listening for anxious")
        // Look for anxious people online and waiting to talk
        FirebaseReferences.anxiousWaiting.observe(.value, with: { (snapshot) in
            // At least one person is online if snapshot exists
            if snapshot.exists() {
                // Check that a call request hasn't been made already
                if Authentication.anxiousID == "" {
                    // Choose one person to connect to
                    // Available methods: "random"
                    let anxiousID = FirebaseSnapshots.chooseAnxious(method: "random", snapshot: snapshot)
                    // Save chosen anxious ID
                    Authentication.anxiousID = anxiousID
                    // Set value to AnxiousID/HelperID = 'request' in database
                    FirebaseWrite.helperSendsCallRequest()
                    print("Chosen anxious ID = \(anxiousID)")
                    // Listen for a response
                    helperListenForCallResponse()
                }
            } else {
                print("No anxious people online")
            }
        })
    }
    
    static func stopListeningForAnxious() {
        print("Stopped listening for anxious")
        FirebaseReferences.anxiousWaiting.removeAllObservers()
    }
    
    static func anxiousListenForCallRequest() {
        print("Listening for call request")
        // Look for call requests from helpers choosing me
        FirebaseReferences.callRequests.child(Authentication.myID).observe(.value, with: { (snapshot) in
            // There's at least one call request if snapshot exists
            if snapshot.exists() {
                // Check that a call request hasn't been made already
                if Authentication.helperID == "" {
                    // Choose one call request to accept
                    // Available methods: "random"
                    let helperID = FirebaseSnapshots.chooseHelper(method: "random", snapshot: snapshot)
                    // Save chosen helper ID
                    Authentication.helperID = helperID
                    // Set value to AnxiousID/HelperID/ 'accepted' in database
                    FirebaseWrite.anxiousAcceptsCallRequest()
                    print("Chosen helper ID = \(helperID)")
                }
            } else {
                print("No helpers have chosen me")
            }
        })
    }
    
    static func stopAnxiousListeningForCallRequest() {
        print("Stopped listening for call request")
        FirebaseReferences.callRequests.child(Authentication.myID).removeAllObservers()
    }
    
    static func helperListenForCallResponse() {
        print("Listening for call response")
        // Listen for a response to call request from anxious person
        FirebaseReferences.callRequests.child(Authentication.anxiousID).child(Authentication.myID).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let response = FirebaseSnapshots.callResponse(snapshot: snapshot)
                print(response)
                if response == "accepted" {
                    print("Helper received accepted response")
                }
            } else {
                print("Call request no longer exists")
            }
        })
    }
    
    static func stopHelperListeningForCallResponse() {
        print("Stop listening for call response")
        if Authentication.anxiousID != "" {
            FirebaseReferences.callRequests.child(Authentication.anxiousID).child(Authentication.myID).removeAllObservers()
        }
    }
    
    static func anxiousObservesHelperCall() {
        FirebaseReferences.helperCall.observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                print(snapshot)
                print("Found helper call")
                
                
            }
        })
    }
    
    static func helperObservesAnxiousCall() {
        FirebaseReferences.anxiousCall.observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                print(snapshot)
                print("Found anxious call")
                
            }
        })
    }
    
    static func stopObservingCall() {
        FirebaseReferences.callRequests.child(Authentication.anxiousID).removeAllObservers()
    }
    
    
    
    
}
