//
//  FirebaseWrite.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 29/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class FirebaseWrite: NSObject {

    static func addHelperToDatabase() {
        FirebaseReferences.helpersWaiting.child(Authentication.myID).setValue("\(Date())")
    }
    
    static func removeHelperFromDatabase() {
        FirebaseReferences.helpersWaiting.child(Authentication.myID).removeValue()
    }
    
    static func addAnxiousToDatabase() {
        FirebaseReferences.anxiousWaiting.child(Authentication.myID).setValue("\(Date())")
        Authentication.anxiousID = Authentication.myID
    }
    
    static func removeAnxiousFromDatabase() {
        FirebaseReferences.anxiousWaiting.child(Authentication.myID).removeValue()
        Authentication.anxiousID = ""
    }
    
    static func helperSendsCallRequest() {
        FirebaseReferences.callRequests.child(Authentication.anxiousID).child(Authentication.myID).setValue("request")
    }
    
    static func helperRemovesCallRequest() {
        FirebaseReferences.callRequests.child(Authentication.anxiousID).child(Authentication.myID).removeValue()
        Authentication.anxiousID = ""
    }
    
    static func anxiousRemovesAllCallRequests() {
        FirebaseReferences.callRequests.child(Authentication.myID).removeValue()
        Authentication.helperID = ""
    }
    
    static func anxiousAcceptsCallRequest() {
        FirebaseReferences.callRequests.child(Authentication.myID).child(Authentication.helperID).setValue("accepted")
    }
    
}
