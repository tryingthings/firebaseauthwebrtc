//
//  Authentication.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 25/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import Firebase

class Authentication: NSObject {
    
    public static let shared = Authentication()

    static var myID = ""
    // When a helper chooses an anxious person, store the anxious ID
    static var anxiousID = ""
    // When an anxious person chooses an helper request to accept, store the helper ID here
    static var helperID = ""

    static func signInAnonymously() {
        // Automatically give an ID of random letters/numbers to the user
        Auth.auth().signInAnonymously() { (authResult, error) in
            if error == nil {
                if let user = authResult?.user {
                    let isAnonymous = user.isAnonymous  // true
                    let uid = user.uid // eg. EGnhg3jvumTbXBiraDv0Tq142KK2
                    print("User ID \(uid) is anonymous: \(isAnonymous)")
                    myID = uid
                }
            } else {
                print("Firebase Auth error: \(String(describing: error))")
            }
        }
    }
}
