//
//  DatabaseReferences.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 25/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import Firebase

class FirebaseReferences: NSObject {
    
    // Make shortcuts for locations in the database
    static let callRequests = Database.database().reference().child("callRequests")
    static let anxiousWaiting = Database.database().reference().child("anxiousWaitingForHelpers")
    static let helpersWaiting = Database.database().reference().child("helpersWaitingForAnxious")
   static let helperCall = Database.database().reference().child("callRequests").child(Authentication.anxiousID).child("callHelper")
    static let anxiousCall = Database.database().reference().child("callRequests").child(Authentication.anxiousID).child("callAnxious")
   
    static func setup() {
        callRequests.onDisconnectRemoveValue()
        anxiousWaiting.onDisconnectRemoveValue()
        helpersWaiting.onDisconnectRemoveValue()    }
}
