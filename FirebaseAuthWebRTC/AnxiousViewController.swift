//
//  AnxiousViewController.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 25/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class AnxiousViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        FirebaseWrite.addAnxiousToDatabase()
        FirebaseObservers.anxiousListenForCallRequest()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        FirebaseWrite.removeAnxiousFromDatabase()
        FirebaseWrite.anxiousRemovesAllCallRequests()
        FirebaseObservers.stopAnxiousListeningForCallRequest()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
