//
//  HelperViewController.swift
//  FirebaseAuthWebRTC
//
//  Created by trying-things on 29/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class HelperViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Add current user to list of available helpers
        FirebaseWrite.addHelperToDatabase()
        // Look for anxious people waiting to talk
        FirebaseObservers.listenForAnxious()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        FirebaseWrite.removeHelperFromDatabase()
        FirebaseObservers.stopListeningForAnxious()
        FirebaseWrite.helperRemovesCallRequest()
        FirebaseObservers.stopHelperListeningForCallResponse()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
